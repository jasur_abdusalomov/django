from django.shortcuts import render
from .models import News
# Create your views here.

def Home(request):
    obj = News.objects.get(id=1)
    context = {
        "data": obj,
        "description":obj,
        'create_at':obj
            }
    return render(request,'index.html',context)